/**
 * Greenhouse automation
 * 
 * Reading soil sensor and start a pump if the soil is too dry.
 * Web server for status info.
 */

#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include "DHT.h"

//constants
/************************* soil sensor *********************************/
const int RELAY_PIN = 12;
const int SENSORPOWER_PIN = 13;
const int SENSOR_PIN = A0;

/************************* DHT sensor *********************************/
#define DHTPIN D4     // DHT pin
#define DHTTYPE DHT22   // DHT 11

/************************* program constants *********************************/
const int PUMP_TIME = 5000; //5000 ms = 5 s
const int MEASURE_INTERVAL = 900; //900 s = 15 min
const int LOG_TOTAL_TIME = 86400;//86400 s = 24 h
const int LOG_SIZE = LOG_TOTAL_TIME / MEASURE_INTERVAL;

/************************* WiFi *********************************/
const char *ssid = "mySSID";
const char *password = "myPassword";

//globals
int moistLimit = 520;
int loopCount = MEASURE_INTERVAL;//measure on first loop
int pumpCount = 0;
long lastCheck = 0;
long lastPump = 0;

int logCount = 0;
float soilLog[LOG_SIZE];
float pumpLog[LOG_SIZE];
float tempLog[LOG_SIZE];
float humidLog[LOG_SIZE];

/** DHT **/
DHT dht(DHTPIN, DHTTYPE);

ESP8266WebServer server(80);

void setup() {
  //setup pins
  pinMode(RELAY_PIN, OUTPUT);
  pinMode(SENSORPOWER_PIN, OUTPUT);

  //setup serial
  Serial.begin(9600);
  delay(1000);
  Serial.println(F("SETUP")); 

  //setup dht
  dht.begin();

  //setup accesspoint
  WiFi.softAP(ssid, password);
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("Server IP address: ");
  Serial.println(myIP);

  //setup server
  server.on("/", showStatus);
  server.on("/showLog", showLog);
  server.on("/pump", actionPump);
  server.on("/settings", actionSettings);
  server.begin();
  Serial.println("Web server started");
}

void loop() {
  server.handleClient();
  handleIO();
  delay(1000);
}

void handleIO() {
  loopCount++;
  if(loopCount > MEASURE_INTERVAL) {
    loopCount = 0;
    Serial.println("Reading soilsensor");
    int soilValue = readSoilSensor();
    Serial.print("Value: ");
    Serial.println(soilValue);
    lastCheck = millis();
    
    if(soilValue > moistLimit) {
      Serial.println("Too dry");
      pumpWater();
      lastPump = millis();
      pumpCount++;
    } else {
      Serial.println("Soil is moist");
    }

    float temperature = dht.readTemperature();
    float humidity = dht.readHumidity();
    Serial.print("Temperature: ");
    Serial.println(temperature);
    Serial.print("Humidity: ");
    Serial.println(humidity);
    logValues(soilValue, pumpCount, temperature, humidity);
  }
}

int readSoilSensor() {
  int bias = analogRead(SENSOR_PIN);
  digitalWrite(SENSORPOWER_PIN, HIGH);
  //wait until power is stable before read sensor
  delay(2000);
  int value = analogRead(SENSOR_PIN);
  digitalWrite(SENSORPOWER_PIN, LOW);
  return value - bias;
}

void pumpWater() {
  Serial.println("Starting pump");
  digitalWrite(RELAY_PIN, HIGH);
  delay(PUMP_TIME);
  Serial.println("Stopping pump");
  digitalWrite(RELAY_PIN, LOW);
}

void logValues(int soilValue, int pumpCount, float temperature, float humidity) {
  int logIndex = logCount % LOG_SIZE;
  soilLog[logIndex] = soilValue;
  pumpLog[logIndex] = pumpCount;
  tempLog[logIndex] = temperature;
  humidLog[logIndex] = humidity;
  logCount++;
}

void showStatus() {
  String head = "<html><head>";
  head += "<title>Autotomato</title>";
  head += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"></head>";
  
  String body = "<body>";
  body += "<a href=\"/\"><h1>Autotomato</h1></a>";

  body += createGraph(soilLog, "Jordresistans", pumpLog, "Pumpningar");
  body += "<br/>";
  body += "<br/>";

  body += createGraph(tempLog, "Temperatur", humidLog, "Luftfuktighet");
  body += "<br/>";
  body += "<br/>";

  body += "<b>Jordresistans:</b> ";
  body += readSoilSensor();
  body += " (gr&auml;ns ";
  body += moistLimit;
  body += ")";
  body += "<br/>";
  body += "<b>Antal pumpningar:</b> ";
  body += pumpCount;
  body += "<br/>";
  body += "<b>Temperatur:</b> ";
  body += dht.readTemperature();
  body += "<br/>";
  body += "<b>Luftfuktighet:</b> ";
  body += dht.readHumidity();
  body += "<br/>";
  body += "<b>Drifttid:</b> ";
  body += msToStr(millis());
  body += "<br/>";
  body += "<b>Senaste m&auml;tning:</b> ";
  body += msToStr(millis() - lastCheck);
  body += "<br/>";
  body += "<b>Senaste pumpning:</b> ";
  body += msToStr(millis() - lastPump);
  body += "<br/>";

  body += "<h2>Kommandon</h2>";
  body += "<form action=\"pump\" method=\"POST\">";
  body += "<input type=\"submit\" value=\"Pumpa vatten\"><br/>";
  body += "</form>";

  body += "<h2>Inst&auml;llningar</h2>";
  body += "<form action=\"settings\" method=\"POST\">";
  body += "<input type=\"number\" name=\"moistlimit\" value=\"";
  body += moistLimit;
  body += "\"><br/>";
  body += "<input type=\"submit\" value=\"Spara\"><br/>";
  body += "</form>";

  body += "<a href=\"/showLog\"><h2>Logg</h2></a>";
  
  body += "</body>";

  String page = head;
  page += body;
  page += "</html>";
  
  
  server.send(200, "text/html", page);
}

void showLog() {
  String head = "<html><head>";
  head += "<title>Autotomato</title>";
  head += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"></head>";
  
  String body = "<body>";
  body += "<a href=\"/\"><h1>Autotomato</h1></a>";

  body += "<h2>Logg</h2>";
  body += createLogTable();
  
  body += "</body>";

  String page = head;
  page += body;
  page += "</html>";
  
  
  server.send(200, "text/html", page);
}

String createGraph(float data1[], String data1Label, float data2[], String data2Label) {
  const int width = 360;
  const int height = width / 2;
  const int leftMargin = 32;
  const int rightMargin = 32;
  const int topMargin = 20;
  const int bottomMargin = 20;
  const String color1 = "#0074d9";
  const String color2 = "#339933";
  const String colorX = "#e6e6e6";
  const String colorXText = "#000000";
  const int baseline = height - bottomMargin;
  const int logsize = LOG_SIZE;

  String svg = "<svg width=\"";
  svg += width;
  svg += "\" height=\"";
  svg += height;
  svg += "\">";

  //x-axis
  svg += "<g>";
  svg += svgLine(leftMargin, baseline, (width - rightMargin), baseline, colorX);
  svg += timeText(logsize, colorXText, width, leftMargin, rightMargin, height-2);
  svg += "</g>";

  //y-axis
  //right
  svg += "<g>";
  svg += svgLine((width - rightMargin), baseline, (width - rightMargin), topMargin, color1);
  svg += minmaxText(data1, logsize, color1, (width-rightMargin + 3), topMargin, baseline);
  svg += svgTextVertical(data1Label, (width-10), (baseline-20), color1);
  svg += "</g>";

  //left
  svg += "<g>";
  svg += svgLine(leftMargin, baseline, leftMargin, topMargin, color2);
  svg += minmaxText(data2, logsize, color2, 2, topMargin, baseline);
  svg += svgTextVertical(data2Label, (leftMargin-13), (baseline-20), color2);
  svg += "</g>";

  //data1
  svg += "<g>";
  svg += lineGraph(data1, logsize, height, width, topMargin, bottomMargin, leftMargin, rightMargin, color1);
  svg += "</g>";

  //data2
  svg += "<g>";
  svg += lineGraph(data2, logsize, height, width, topMargin, bottomMargin, leftMargin, rightMargin, color2);
  svg += "</g>";

  svg += "</svg>";
  return svg;
}

String timeText(int logsize, String color, int width, int leftMargin, int rightMargin, int y) {
  const float graphWidth = width - leftMargin - rightMargin;
  const int eachStep = 3*4; 
  const float xStep = eachStep * graphWidth / logsize;

  String text = "";
  int t = 0;
  float x = width - rightMargin;
  for(int i = 0; i <= logsize; i += eachStep) {
    text += svgText(String(t), x, y, color);
    x -= xStep;
    t -= 3;
  }
  return text;
}

String minmaxText(float data[],int logsize, String color, int x, int top, int bottom) {
  int count = logCount - 1;
  int logIndex = count % logsize;
  float minValue = data[logIndex];
  float maxValue = data[logIndex];
  for(int i = 0; i < logsize && logIndex >= 0; i++) {
    float value = data[logIndex];
    if(!isnan(value)) {
      if(value < minValue) {
        minValue = value;
      }
      if(value > maxValue) {
        maxValue = value;
      }
    }
    count--;
    logIndex = count % logsize;
  }
  String s = svgText(floatToString(maxValue), x, top, color);
  s += svgText(floatToString(minValue), x, bottom, color);
  return s;
}

String lineGraph(float data[], int logsize, int height,int  width, int topMargin, int bottomMargin, int leftMargin, int rightMargin, String color) {
  String t = "<polyline fill=\"none\" stroke=\"";
  t += color;
  t += "\" stroke-width=\"2\" points=\"";

  int count = logCount - 1;
  int logIndex = count % logsize;
  float minValue = data[logIndex];
  float maxValue = data[logIndex];
  for(int i = 0; i < logsize && logIndex >= 0; i++) {
    float value = data[logIndex];
    if(!isnan(value)) {
      if(value < minValue) {
        minValue = value;
      }
      if(value > maxValue) {
        maxValue = value;
      }
    }
    count--;
    logIndex = count % logsize;
  }
  const float dataInterval = maxValue - minValue;
  const float graphWidth = width - leftMargin - rightMargin;
  const float graphHeight = height - topMargin - bottomMargin;
  const int baseline = height - bottomMargin;
  const float xStep = graphWidth / logsize;
  const float yStep = graphHeight / dataInterval;
  float x = width - rightMargin;
  count = logCount - 1;
  logIndex = count % logsize;
  for(int i = 0; i < logsize && logIndex >= 0; i++) {
    if(!isnan(data[logIndex])) {
      float value = yStep * (data[logIndex] - minValue);
      t += String(x);
      t += ",";
      t += (baseline - value);
      t += " ";
    }
    x -= xStep;
    count--;
    logIndex = count % logsize;
  }
  
  t += "\"/>";
  return t;
}

String svgText(String text, int x, int y, String color) {
  String t = "<text x=\"";
  t += x;
  t += "\" y=\"";
  t += y;
  t += "\" fill=\"";
  t += color;
  t += "\">";
  t += text;
  t += "</text>";
  return t;
}

String svgTextVertical(String text, int x, int y, String color) {
  String t = "<text x=\"";
  t += x;
  t += "\" y=\"";
  t += y;
  t += "\" fill=\"";
  t += color;
  t += "\" transform=\"rotate(270 ";
  t += x;
  t += ",";
  t += y;
  t += ")\">";
  t += text;
  t += "</text>";
  return t;
}

String svgLine(int x1, int y1, int x2, int y2, String color) {
  String t = "<line fill=\"none\" stroke=\"";
  t += color;
  t += "\" stroke-width=\"2\" ";
  t += "x1=\"";
  t += x1;
  t += "\" y1=\"";
  t += y1;
  t += "\" x2=\"";
  t += x2;
  t += "\" y2=\"";
  t += y2;
  t += "\"></line>";
  return t;
}

String createLogTable() {
  String table = "<table>";
  table += "<tr><th>i</th><th>logIndex</th><th>soilvalue</th><th>pumpcount</th></th><th>temp</th></th><th>humid</th></tr>";
  int count = logCount - 1;
  for(int i = 0; i < LOG_SIZE; i++) {
    float soilvalue = 0;
    float pumpcount = 0;
    float temp = 0;//TODO handle negative temperatures
    float humid = 0;
    int logIndex = count % LOG_SIZE;
    if(logIndex >= 0) {
      soilvalue = soilLog[logIndex];
      pumpcount = pumpLog[logIndex];
      temp = tempLog[logIndex];
      humid = humidLog[logIndex];
    }
    String rowstyle = "";
    if(i % 4 == 0) {
      rowstyle = "background-color: #f2f2f2;";
    }
    table += "<tr style=\"";
    table += rowstyle;
    table += "\"><td>";
    table += i;
    table += "</td><td>";
    table += logIndex;
    table += "</td><td>";
    table += soilvalue;
    table += "</td><td>";
    table += pumpcount;
    table += "</td><td>";
    table += temp;
    table += "</td><td>";
    table += humid;
    table += "</td></tr>";
    count--;
  }
  table += "</table>";
  return table;
}

void actionPump() {
  pumpWater();
  lastPump = millis();
  pumpCount++;
  showStatus();
}

void actionSettings() {
  String arg1 = server.arg("moistlimit");
  moistLimit = arg1.toInt();
  showStatus();
}

/*
1 decimal unless a 0 as decimal, then no decimals
*/
String floatToString(float f) {
  String s = String(f, 1);
  int len = s.length();
  if(s.endsWith(".0")) {
    s = s.substring(0, len - 2);
  }
  return s;
}

String msToStr(long currentmillis) {
  long days=0;
  long hours=0;
  long mins=0;
  long secs=0;
  
  secs = currentmillis/1000; //convect milliseconds to seconds
  mins=secs/60; //convert seconds to minutes
  hours=mins/60; //convert minutes to hours
  days=hours/24; //convert hours to days
  secs=secs-(mins*60); //subtract the coverted seconds to minutes in order to display 59 secs max
  mins=mins-(hours*60); //subtract the coverted minutes to hours in order to display 59 minutes max
  hours=hours-(days*24); //subtract the coverted hours to days in order to display 23 hours max
  
  String str = "";
  str += days;
  str += " d ";
  str += hours;
  str += ":";
  str += mins;
  str += ":";
  str += secs;
  return str;
}

