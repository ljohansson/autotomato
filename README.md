# Autotomato #

Arduino code for greenhouse automation

* Reading soil sensor and start a pump if the soil is too dry.
* Thermometer and hygrometer
* Web server for status info, graphs and config.

## Dependencies ##

DHT 22
[Adafruit_Sensor](https://github.com/adafruit/Adafruit_Sensor)
[DHT-sensor-library](https://github.com/adafruit/DHT-sensor-library)



## Schema ##
![Schema](docs/schema1.png)

![Schema](docs/schema2.png)


## Screenshot ##
![Screenshot](docs/screenshot.png)

## Images ##
![Logic](docs/logic.jpg)